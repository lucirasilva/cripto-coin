# cripto-coin

## Description
Package to get current cripto currency quote and price conversion using [CoinMarketCap](https://coinmarketcap.com/api/).

## Usage
APIKEY is the authorization to handle CoinMarketCap.

- Get cripto currency quote

```javascript
import { CoinMarketCap } from "cripto-coin"

const lib = new CoinMarketCap();

const response = lib.getCurrentQuote(["BTC", "ETH"], APIKEY)
```

- Get price conversion

```javascript
import { CoinMarketCap } from "cripto-coin"

const lib = new CoinMarketCap();

const response = lib.getPriceConversion("BTC", 0.005, ["ETH"], APIKEY)
```

Return of current quote:
```javascript
{
  data: {
    BTC: {
      id: 1,
      name: 'Bitcoin',
      symbol: 'BTC',
      date_added: '2013-04-28T00:00:00.000Z',
      last_updated: '2021-12-17T14:31:02.000Z',
      quote: [Object]
    }
  }
}
```

Return of price conversion:
```javascript
{
  id: 1,
  symbol: 'BTC',
  name: 'Bitcoin',
  amount: 0.005,
  last_updated: '2021-12-17T14:35:02.000Z',
  quote: {
    ETH: {
      price: 0.062021182669447145,
      last_updated: '2021-12-17T14:35:02.000Z'
    }
  }
}
```