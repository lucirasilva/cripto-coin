import {
  Coin,
  CoinMarketCapError,
  ConversionResponse,
  Response,
} from "../types/coinMarketCap";

import axios, { AxiosInstance } from "axios";


export class CoinMarketCap {
  baseURL: string = "https://pro-api.coinmarketcap.com";
  axiosInstance: AxiosInstance;

  constructor() {
    this.axiosInstance = axios.create({
      baseURL: this.baseURL,
    });
  }
  filterDataInfo(data: Coin, coins: Array<string>) {
    const filteredData = { data: {} };

    for (let type in coins) {
      const currentCoin = coins[type];
      const {
        id,
        name,
        symbol,
        date_added,
        last_updated,
        quote,
        ...unecessaryData
      } = data[currentCoin];

      const coin: Coin = {
        [currentCoin]: { id, name, symbol, date_added, last_updated, quote },
      };
      filteredData["data"] = { ...filteredData["data"], ...coin };
    }
    return filteredData;
  }
  async getCurrentQuote(coins: Array<string>, apiKey: string) {
    try {
      const { data } = await this.axiosInstance.get(
        `/v1/cryptocurrency/quotes/latest?symbol=${coins}`,
        {
          headers: {
            "X-CMC_PRO_API_KEY": apiKey,
          },
        }
      );
      const info = this.filterDataInfo(data["data"], coins);

      return info as Response;
    } catch (error) {
      if (axios.isAxiosError(error)) {
        return error.response?.data as CoinMarketCapError;
      }
    }
  }
  async getPriceConversion(
    sourceCoin: string,
    amount: number,
    targetCoins: Array<string>,
    apiKey: string
  ) {
    try {
      const { data } = await this.axiosInstance.get(
        `/v1/tools/price-conversion?symbol=${sourceCoin}&amount=${amount}&convert=${targetCoins}`,
        {
          headers: {
            "X-CMC_PRO_API_KEY": apiKey,
          },
        }
      );
      return data.data as ConversionResponse;
    } catch (error) {
      if (axios.isAxiosError(error)) {
        return error.response?.data as CoinMarketCapError;
      }
    }
  }
}
