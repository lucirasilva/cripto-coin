const axios = require("axios").default;
const dotenv = require("dotenv")
dotenv.config();
const BASE_URL = "https://pro-api.coinmarketcap.com";


axios.get(`${BASE_URL}/v1/cryptocurrency/quotes/latest`, {
    headers: {
        "X-CMC_PRO_API_KEY": process.env.API_KEY
    }
})
    .then(response => response)
    .catch(e => console.log(e));