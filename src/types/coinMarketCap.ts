export interface CoinTypeQuote {
  price: number;
  last_updated: Date;
}

export interface Quote {
  [quoteCoin: string]: CoinTypeQuote;
}

export interface Coin {
  [coin: string]: CoinInfo;
}
export interface Response {
  data: Coin;
}

export interface BaseCoinInfo {
  id: number;
  name: string;
  symbol: string;
  last_updated: Date;
  quote: Quote;
}
export interface ConversionCoinData extends BaseCoinInfo {
  amount: number;
}
export interface CoinInfo extends BaseCoinInfo {
  date_added: Date;
}

export interface ConversionResponse {
  data: ConversionCoinData;
}

export interface CoinMarketCapError {
  detail: string;
}

export const isCoinMarketCapError = (
  error: any
): error is CoinMarketCapError => {
  return (error as CoinMarketCapError).detail !== undefined;
};
